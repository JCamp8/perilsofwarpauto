/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package perilsofwarp;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

/**
 *
 * @author telok
 */
public class PerilsOfWarp {


    private static Scanner keyIn = new Scanner(System.in);
    private static Random randy = new Random();
    
    public static void main(String[] args) 
    {       
        ArrayList<String> minorArray = new ArrayList<>();
        ArrayList<String> lesserArray = new ArrayList<>();
        ArrayList<String> greaterArray = new ArrayList<>();
        ArrayList<String> deadlyArray = new ArrayList<>();
        
        try
        {
            Scanner minor = new Scanner(new File("minorPerils"));
            Scanner lesser = new Scanner(new File("lesserPerils"));
            Scanner greater = new Scanner(new File("greaterPerils"));
            Scanner deadly = new Scanner(new File("deadlyPerils"));
            
            while(minor.hasNextLine())
            {
                minorArray.add((minor.nextLine()));
            }
            while(lesser.hasNextLine())
            {
                lesserArray.add((lesser.nextLine()));
            }
            while(greater.hasNextLine())
            {
                greaterArray.add((greater.nextLine()));
            }
            while(deadly.hasNextLine())
            {
                deadlyArray.add((deadly.nextLine()));
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("Error: " + e.getMessage());
            System.exit(0);
        }
        
        ArrayList<String>[] listArray = new ArrayList[4];
        listArray[0] = minorArray;
        listArray[1] = lesserArray;
        listArray[2] = greaterArray;
        listArray[3] = deadlyArray;

        // exit by crashing
        while(true)
        {
            System.out.println("Enter the modifier number (5, 10, 20, 375, etc.)");
            String answer = keyIn.nextLine();
            randy.setSeed(System.currentTimeMillis()); // yes, I'm lazy
            rolls(answer, listArray, 0, 0);
        }
        
    }
    
    private static int rollDice(int number, int size)
    {
        int output = 0;
        for(int i = 0; i < number; i++)
        {
            output = output + randy.nextInt(size) + 1;
        }
        //System.out.println("debug: rollDice: " + number + "d" + size + ": result = " + output);
        return output;
    }
    
    private static void rolls(String input, ArrayList<String>[] listArray,
            int limiter, int listToUse)
    {
        if(limiter < 7) // because seriously
        {
            // get the count of items in the list
            int countOfItems = listArray[listToUse].size();
            float foo1 = (float)countOfItems/(float)100;
            float foo2 = foo1*(float)Integer.decode(input);
            int modifier = Math.round( foo2 );
            int lineIndex = randy.nextInt(countOfItems)+1;
            lineIndex = lineIndex + modifier;

            // if above > count then above = count
            if (lineIndex >= countOfItems)
            {
                lineIndex = countOfItems-1;
            }

            // get item
            String lineItem = listArray[listToUse].get(lineIndex);
            
            // parse item
            
            // roll twice
            if(lineItem.contains("Roll twice on this table"))
            {
                limiter++;
                rolls(input, listArray, limiter, listToUse);
                rolls("0", listArray, limiter, listToUse);
            }
            
            // next table
            else if(lineItem.contains("Roll the next table and reduce the modifier by 10"))
            {
                int newInput = Integer.decode(input) - 10;
                if(newInput > 0){
                    input = Integer.toString(newInput);
                }
                else{
                    input = "0";
                }
                limiter++;
                if(listToUse < 3)
                {
                    listToUse++;
                }
                else if(listToUse > 3)
                {
                    listToUse = 3;
                }
                //System.out.println("debug: nextTable: from = " + listToUse + ", to = " + listToUse);
                rolls(input, listArray, limiter, listToUse);
            }
            else if(lineItem.contains("Roll 1d10. If the roll is less than 8 roll twice on the preceding table, on 8 to 10:"))
            {
                if(rollDice(1, 10) >= 8)
                {
                    lineItem = lineItem.replace("Roll 1d10. If the roll is less than 8 roll twice on the preceding table, on 8 to 10:", "").trim();
                    // replace XdY dice rolls
                    String[] splitOut = lineItem.split(" ");
                    for(int i = 0; i < splitOut.length; i++)
                    {
                        try
                        {
                            splitOut[i] = splitOut[i].replace(".", "");
                            splitOut[i] = splitOut[i].replace("(", "");
                            if(Pattern.matches("[0-9]+[dD][0-9]+", splitOut[i]))
                            {
                                int dIndex = splitOut[i].toLowerCase().indexOf("d");
                                int number = Integer.decode(splitOut[i].substring(0, dIndex));
                                int size = Integer.decode(splitOut[i].substring(dIndex+1));
                                lineItem = lineItem.replaceFirst(splitOut[i], Integer.toString(rollDice(number, size)));
                            }
                        }
                        catch(Exception e)
                        {
                            System.out.println(lineItem);
                        }
                    }

                    System.out.println(lineItem);
                }
                else 
                {
                    listToUse--;
                    limiter++;
                    rolls(input, listArray, limiter, listToUse);
                    int newInput = Integer.decode(input) - 10;
                    if(newInput > 0){
                        input = Integer.toString(newInput);
                    }
                    else{
                        input = "0";
                    }
                    rolls(input, listArray, limiter, listToUse);
                }
            }
            else
            {
                // replace XdY dice rolls
                String[] splitOut = lineItem.split(" ");
                for(int i = 0; i < splitOut.length; i++)
                {
                    try
                    {
                        splitOut[i] = splitOut[i].replace(".", "");
                        splitOut[i] = splitOut[i].replace("(", "");
                        if(Pattern.matches("[0-9]+[dD][0-9]+", splitOut[i]))
                        {
                            int dIndex = splitOut[i].toLowerCase().indexOf("d");
                            int number = Integer.decode(splitOut[i].substring(0, dIndex));
                            int size = Integer.decode(splitOut[i].substring(dIndex+1));
                            lineItem = lineItem.replaceFirst(splitOut[i], Integer.toString(rollDice(number, size)));
                        }
                    }
                    catch(Exception e)
                    {
                        System.out.println(lineItem);
                    }
                }

                System.out.println(lineItem);
            }
        }
    }
    
}
